﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services
{
    public class PromoCodesService
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IPromoCodesDictionaryGateway _cacheGateway;
        private string key = $"{typeof(PromoCodesService).FullName}_promoCodes";

        public PromoCodesService(IRepository<PromoCode> promoCodesRepository, IPromoCodesDictionaryGateway cacheGateway)
        {
            _promoCodesRepository = promoCodesRepository;
            _cacheGateway = cacheGateway;
        }

        public async Task<PromoCode> GetPromoCodeById(Guid id)
        {
            var result = await _cacheGateway.GetById(id, key);
            if (result != null) return result;
            await UpdateCache();
            return await _promoCodesRepository.GetByIdAsync(id);
        }

        public async Task<PromoCode> GetPromoCodeByCodeAndPartner(string code, Guid partnerId)
        {
            var result = await _cacheGateway.GetByCodeAndPartner(code, partnerId, key);
            if (result != null) return result;
            await UpdateCache();
            return await _promoCodesRepository.GetFirstWhere(q => q.Code == code && q.PartnerId == partnerId);
        }

        public async Task<IEnumerable<PromoCode>> GetPromoCodesByPartnerId(Guid partnerId)
        {
            var result = await _cacheGateway.GetByPartnerId(partnerId, key);
            if (result.Any()) return result;
            await UpdateCache();
            return (await _promoCodesRepository.GetWhere(q => q.PartnerId == partnerId));
        }

        private async Task UpdateCache()
        {
            var allPromoCodes = await _promoCodesRepository.GetAllAsync();
            await _cacheGateway.SetCache(allPromoCodes, key);
        }
    }
}
