using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.DictionaryService.WebHost.Models;
using System.Text.Json;

namespace Otus.Teaching.Pcf.DictionaryService.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IDistributedCache _distributedCache;

        public PromocodesController(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }


        /// <summary>
        /// �������� ������ �����-�����
        /// </summary>
        /// <returns></returns>
        [HttpGet("list")]
        public async Task<ActionResult<IEnumerable<PromocodeItem>>> GetPromoCodesAsync(string key)
        {
            var serialized = await _distributedCache.GetStringAsync(key);
            var response = new List<PromocodeItem>();
            if (serialized != null)
            {
                var promoCodes = JsonSerializer.Deserialize<IEnumerable<PromocodeItem>>(serialized);
                if (promoCodes != null) response.AddRange(promoCodes);
            }

            return Ok(response);
        }

        /// <summary>
        /// �������� �����-��� �� Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("byId/{id}")]
        public async Task<ActionResult<PromocodeItem>> GetPromoCodeAsync(Guid id, string key)
        {
            var serialized = await _distributedCache.GetStringAsync(key);
            PromocodeItem? response = null;
            if (serialized != null)
            {
                var promoCodes = JsonSerializer.Deserialize<IEnumerable<PromocodeItem>>(serialized);
                response = promoCodes?.FirstOrDefault(q => q.Id == id);
            }

            if (response == null) return NotFound();
            return Ok(response);
        }

        /// <summary>
        /// �������� �����-��� �� ����
        /// </summary>
        /// <returns></returns>
        [HttpGet("byCodeAndPartner/{code}")]
        public async Task<ActionResult<PromocodeItem>> GetPromoCodeByCodeAsync(string code, Guid partnerId, string key)
        {
            var serialized = await _distributedCache.GetStringAsync(key);
            PromocodeItem? response = null;
            if (serialized != null)
            {
                var promoCodes = JsonSerializer.Deserialize<IEnumerable<PromocodeItem>>(serialized);
                response = promoCodes?.FirstOrDefault(q => q.Code == code && q.PartnerId == partnerId);
            }

            if (response == null) return NotFound(null);
            return Ok(response);
        }

        /// <summary>
        /// �������� ������ �����-�����
        /// </summary>
        /// <param name="promoCodeIds">guids splitted by ;</param>
        /// <returns></returns>
        [HttpGet("byPartnerId/{partnerId}")]
        public async Task<ActionResult<IEnumerable<PromocodeItem>>> GetPromoCodesByPartnerIdAsync(Guid partnerId, string key)
        {
            var serialized = await _distributedCache.GetStringAsync(key);
            var response = new List<PromocodeItem>();
            if (serialized != null)
            {
                var promoCodes = JsonSerializer.Deserialize<IEnumerable<PromocodeItem>>(serialized);
                if (promoCodes != null)response.AddRange(promoCodes.Where(q => q.PartnerId == partnerId));
            }

            return Ok(response);
        }

        /// <summary>
        /// ���������� ���
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> SetCache(SetCacheRequest<PromocodeItem> request)
        {
            await _distributedCache.SetStringAsync(
                key: request.Key,
                value: JsonSerializer.Serialize(request.Items),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromSeconds(10)
                }
                );
            return Ok();
        }
    }
}