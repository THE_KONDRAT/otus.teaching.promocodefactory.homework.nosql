﻿namespace Otus.Teaching.Pcf.DictionaryService.WebHost.Models
{
    public class SetCacheRequest<T>
    {
        public string Key { get; set; } = default!;

        public IEnumerable<T> Items { get; set; } = new List<T>();
    }
}
