﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface IPromoCodesDictionaryGateway
    {
        public Task<PromoCode> GetById(Guid id, string key);
        public Task<PromoCode> GetByCodeAndPartner(string code, Guid partnerId, string key);
        public Task<IEnumerable<PromoCode>> GetByPartnerId(Guid partnerId, string key);

        public Task SetCache(IEnumerable<PromoCode> promoCodes, string key);
    }
}