﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.GatewaysModels
{
    public class CreateOrEditPromoCodeRequest
    {
        public Guid PromoCodeId { get; set; }
        public string ServiceInfo { get; set; }
        public string PromoCode { get; set; }
        public Guid PartnerId { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
