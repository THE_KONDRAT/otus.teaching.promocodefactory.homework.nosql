﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration.Dto
{
    public class PromoCodeItem
    {
        public Guid Id { get; set; }
        public string Code { get; set; }

        [AllowNull]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid PartnerId { get; set; }

        public Guid? PartnerManagerId { get; set; }

        public Guid PreferenceId { get; set; }
    }
}
