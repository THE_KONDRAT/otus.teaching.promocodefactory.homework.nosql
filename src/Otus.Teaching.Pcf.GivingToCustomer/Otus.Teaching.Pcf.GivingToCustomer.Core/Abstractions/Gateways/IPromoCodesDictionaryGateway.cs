﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways
{
    public interface IPromoCodesDictionaryGateway
    {
        Task<IEnumerable<PromoCode>> GetAll(string key);
        
        public Task SetCache(IEnumerable<PromoCode> promoCodes, string key);
    }
}
