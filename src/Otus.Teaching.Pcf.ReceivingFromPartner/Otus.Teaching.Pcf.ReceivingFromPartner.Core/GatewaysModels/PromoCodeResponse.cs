﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.GatewaysModels
{
    public class PromoCodeResponse
    {
        public Guid Id { get; set; }

        public string Code { get; set; } = default!;

        public string ServiceInfo { get; set; } = default!;

        public DateTime BeginDate { get; set; } = default!;

        public DateTime EndDate { get; set; } = default!;

        public Guid PartnerId { get; set; }
    }
}
