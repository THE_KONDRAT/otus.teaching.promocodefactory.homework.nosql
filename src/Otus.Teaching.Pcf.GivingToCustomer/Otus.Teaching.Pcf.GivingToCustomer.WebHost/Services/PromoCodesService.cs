﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using System.Linq;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public class PromoCodesService
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IPromoCodesDictionaryGateway _cacheGateway;
        private string key = $"{typeof(PromoCodesService).FullName}_promoCodes";

        public PromoCodesService(IRepository<PromoCode> promoCodesRepository, IPromoCodesDictionaryGateway cacheGateway)
        {
            _promoCodesRepository = promoCodesRepository;
            _cacheGateway = cacheGateway;
        }

        public async Task<IEnumerable<PromoCode>> GetAll()
        {
            var result = await _cacheGateway.GetAll(key);
            if (result.Any()) return result;
            await UpdateCache();
            result = await _promoCodesRepository.GetAllAsync();
            return result;
        }

        public async Task AddAsync(PromoCode promoCode)
        {
            await _promoCodesRepository.AddAsync(promoCode);
        }
        
        private async Task UpdateCache()
        {
            var allPromoCodes = await _promoCodesRepository.GetAllAsync();
            await _cacheGateway.SetCache(allPromoCodes, key);
        }
    }
}
