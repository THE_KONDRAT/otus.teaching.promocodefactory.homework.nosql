﻿using System;
using System.Collections.Generic;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.GatewaysModels;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

 namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeRequest request, Preference preference, IEnumerable<Customer> customers)
        {
            var result = new PromoCode();

            Guid id;
            if (!Guid.TryParse(request.PromoCode, out id)) id = Guid.NewGuid();

            result.Id = id;
            result.PartnerId = request.PartnerId;
            result.Code = request.PromoCode;
            result.ServiceInfo = request.ServiceInfo;

            result.BeginDate = DateTime.Now;
            result.EndDate = DateTime.Now.AddDays(30);
            result.Preference = preference;
            result.PreferenceId = preference.Id;

            result.Customers = new List<PromoCodeCustomer>();
            foreach (var customer in customers)
            {
                result.Customers.Add(new PromoCodeCustomer { 
                    PromoCode = result, 
                    PromoCodeId = result.Id, 
                    Customer = customer, 
                    CustomerId = customer.Id 
                });
            }

            return result;
        }

        public static CreateOrEditPromoCodeRequest MapRequestFromModel(GivePromoCodeRequest request, Preference preference, IEnumerable<Customer> customers)
        {
            
            var result = new CreateOrEditPromoCodeRequest();

            Guid id;
            if (!Guid.TryParse(request.PromoCode, out id)) id = Guid.NewGuid();

            result.PromoCodeId = id;
            result.PartnerId = request.PartnerId;
            result.PromoCode = request.PromoCode;
            result.ServiceInfo = request.ServiceInfo;

            result.BeginDate = DateTime.Now;
            result.EndDate = DateTime.Now.AddDays(30);

            //promocode.PreferenceId = preference.Id;

            //promocode.PartnerManagerId = request.PartnerManagerId;

            return result;
        }
    }
}
