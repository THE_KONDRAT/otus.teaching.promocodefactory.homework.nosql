﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class PromoCodesDictionaryGateway
        : IPromoCodesDictionaryGateway
    {
        private readonly HttpClient _httpClient;
        private readonly JsonSerializerOptions jsonSerializerOptions;

        public PromoCodesDictionaryGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
            jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
        }

        public async Task<PromoCode> GetById(Guid id, string key)
        {
            var response = await _httpClient.GetAsync($"api/v1/promocodes/byId/{id}?key={key}");
            if (!response.IsSuccessStatusCode) return null;
            var promocodeItem = await JsonSerializer.DeserializeAsync<PromoCodeItem>(await response.Content.ReadAsStreamAsync(), jsonSerializerOptions);
            return new PromoCode
            {
                Id = promocodeItem.Id,
                Code = promocodeItem.Code,
                BeginDate = promocodeItem.BeginDate,
                EndDate = promocodeItem.EndDate,
                PartnerId = promocodeItem.PartnerId,
                PartnerManagerId = promocodeItem.PartnerManagerId,
                PreferenceId = promocodeItem.PreferenceId,
                ServiceInfo = promocodeItem.ServiceInfo,
            };
        }

        public async Task<PromoCode> GetByCodeAndPartner(string code, Guid partnerId, string key)
        {
            var response = await _httpClient.GetAsync($"api/v1/promocodes/byCodeAndPartner/{code}?partnerId={partnerId}&key={key}");
            if (!response.IsSuccessStatusCode) return null;
            var promocodeItem = await JsonSerializer.DeserializeAsync<PromoCodeItem>(await response.Content.ReadAsStreamAsync(), jsonSerializerOptions);
            return new PromoCode
            {
                Id = promocodeItem.Id,
                Code = promocodeItem.Code,
                BeginDate = promocodeItem.BeginDate,
                EndDate = promocodeItem.EndDate,
                PartnerId = promocodeItem.PartnerId,
                PartnerManagerId = promocodeItem.PartnerManagerId,
                PreferenceId = promocodeItem.PreferenceId,
                ServiceInfo = promocodeItem.ServiceInfo,
            };
        }

        public async Task<IEnumerable<PromoCode>> GetByPartnerId(Guid partnerId, string key)
        {
            var response = await _httpClient.GetAsync($"api/v1/promocodes/byPartnerId/{partnerId}?key={key}");
            var promocodeItems = await JsonSerializer.DeserializeAsync<IEnumerable<PromoCodeItem>>(await response.Content.ReadAsStreamAsync(), jsonSerializerOptions);
            return promocodeItems.Select(q => new PromoCode
            {
                Id = q.Id,
                Code = q.Code,
                BeginDate = q.BeginDate,
                EndDate = q.EndDate,
                PartnerId = q.PartnerId,
                PartnerManagerId = q.PartnerManagerId,
                PreferenceId = q.PreferenceId,
                ServiceInfo = q.ServiceInfo,
            });
        }

        public async Task SetCache(IEnumerable<PromoCode> promoCodes, string key)
        {
            var dto = new PromoCodeCacheDto
            {
                Key = key,
                Items = promoCodes.Select(q => new PromoCodeItem
                {
                    Id = q.Id,
                    Code = q.Code,
                    BeginDate = q.BeginDate,
                    EndDate = q.EndDate,
                    PreferenceId = q.PreferenceId,
                    PartnerId = q.PartnerId,
                    PartnerManagerId = q.PartnerManagerId,
                    ServiceInfo = q.ServiceInfo,
                })
            };

            var response = await _httpClient.PostAsJsonAsync("api/v1/promocodes", dto);
        }
    }
}