﻿using System.Collections.Generic;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public class PromoCodeCacheDto
    {
        public string Key { get; set; }

        public IEnumerable<PromoCodeItem> Items { get; set; } = new List<PromoCodeItem>();
    }
}