﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.GatewaysModels;
using Otus.Teaching.Pcf.GivingToCustomer.Integration.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class PromoCodesDictionaryGateway
        : IPromoCodesDictionaryGateway
    {
        private readonly HttpClient _httpClient;
        private readonly JsonSerializerOptions jsonSerializerOptions;

    public PromoCodesDictionaryGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
            jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
        }

        public async Task<IEnumerable<PromoCode>> GetAll(string key)
        {
            var response = await _httpClient.GetAsync($"api/v1/promocodes/list?key={key}");
            var promocodeItems = await JsonSerializer.DeserializeAsync<List<PromoCodeItem>>(await response.Content.ReadAsStreamAsync(), jsonSerializerOptions);
            return promocodeItems.Select(q => new PromoCode
            {
                Id = q.Id,
                Code = q.Code,
                BeginDate = q.BeginDate,
                EndDate = q.EndDate,
                PartnerId = q.PartnerId,
                PreferenceId = q.PreferenceId,
                ServiceInfo = q.ServiceInfo,
            });
        }

        public async Task SetCache(IEnumerable<PromoCode> promoCodes, string key)
        {
            var dto = new PromoCodeCacheDto
            {
                Key = key,
                Items = promoCodes.Select(q => new PromoCodeItem
                {
                    Id = q.Id,
                    Code = q.Code,
                    BeginDate = q.BeginDate,
                    EndDate = q.EndDate,
                    PreferenceId = q.PreferenceId,
                    PartnerId = q.PartnerId,
                    PartnerManagerId = null,
                    ServiceInfo = q.ServiceInfo,
                })
            };

            var response = await _httpClient.PostAsJsonAsync("api/v1/promocodes", dto);
        }
    }
}
